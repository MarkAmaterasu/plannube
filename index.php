<?php
	require 'aplicacion/_configuracion/config.php';
	// Averiguamos el idioma del navegador del usuario.
	$idiomas = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
	$idiomaNav = substr( $idiomas, 0, 2);

	// En el caso de que no le pasemos nada, por defecto será Index/index
	$url = isset($_GET["url"]) ? $_GET["url"] : $idiomaNav."/Index/index";

	$url = explode("/", $url); // Crea un array

	// Comprobamos que existan y los guardamos en unas variables.
	if (isset($url[0])) {
		$idioma = $url[0];
	}
	if (isset($url[1])) {
		$controlador = $url[1];
	}
	if (isset($url[2])) {
		if ($url[2] != "") {
			$metodo = $url[2];
		}		
	}
	if (isset($url[3])) {
		if ($url[3] != "") {
			$parametro = $url[3];
		}
	}

	// Determina el idioma de la página según el parámetro de la url pasado.
	if ($idioma == "es") {
		include(IDIOMA."es-ES.php");
		$GLOBALS['language'] = $idioma;
	}else if ($idioma == "ca"){
		include(IDIOMA."ca.php");
		$GLOBALS['language'] = $idioma;
	}else if($idioma == "en"){
		include(IDIOMA."en-GB.php");
		$GLOBALS['language'] = $idioma;
	}else{
		include(IDIOMA."en-GB.php");
		$GLOBALS['language'] = $idioma;
	}

	// Determina si la clase está cargada o no.
	// La clave de esto, es que el archivo tenga el mismo nombre que la clase.
	spl_autoload_register(function($clase){
		// Y si existe el archivo, la carga.
		if (file_exists(LIB.$clase.".php")) {
			require LIB.$clase.'.php';
		}
	});

	// Determina si el controlador existe o no y lo carga.
	if (file_exists(CONTROLADOR.$controlador.'.php')) {
		require CONTROLADOR.$controlador.'.php';
		$controlador = new $controlador();

		if (isset($metodo)) {
			if (method_exists($controlador, $metodo)) {
				if (isset($parametro)) {
					$controlador->{$metodo}($parametro);
				}else{
					$controlador->{$metodo}();
				}
			}
		}else{
			$controlador->index();
		}
	}else{
		echo "¡Error al cargar el método!";
	}	
?>