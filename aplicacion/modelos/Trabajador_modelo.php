<?php
	Class Trabajador_modelo extends Modelo{

		function __construct(){
			parent::__construct();
		}

		// Recoge los datos de un usuario en particular.
		function trabajador($id){
			return $this->db->select("SELECT * FROM trabajador WHERE idtrabajador=:id",array("id"=>$id));
		}
	}
?>