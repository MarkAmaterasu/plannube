<?php

	class Empresa_modelo extends Modelo{

		function __construct(){
			parent::__construct();
		}

		// Hace el Insert de la empresa.
		function registro($datos){
			return $this->db->insert('trabajador',$datos);
		}

		// Recoge los datos apra poder compararlos con el ususario que hace login.
		function entrar($datos){
			return $this->db->select("SELECT * FROM trabajador WHERE email = :email",$datos);			
		}

		// Hace el insert del trabajador.
		function registrarTrabajador($datos){
			return $this->db->insert('trabajador',$datos);
		}

		// Hace el Select de los trabajadores para poder listarlos.
		function listarTrabajadores($datos){
			return $this->db->select("SELECT * FROM trabajador WHERE empresa = :idempresa",array("idempresa"=>$datos));			
		}

		// Recoge los datos de un usuario en particular.
		function trabajador($id){
			return $this->db->select("SELECT * FROM trabajador AS t INNER JOIN rol AS r ON t.rol = r.idrol JOIN grupo AS g ON t.grupo = g.idgrupo WHERE idtrabajador=:id",array("id"=>$id));
		}

		// Hace el Update de un usuario en particular.
		function actualizarTrabajador($id,$datos){
			return $this->db->update("trabajador",$datos,"idtrabajador=".$id);
		}

		// Recoge los datos de una empresa en particular
		function empresa($id){
			return $this->db->select("SELECT * FROM empresa WHERE idempresa = :id",array("id"=>$id));
		}

		// Hace el Select de los grupos de trabajadores para poder listarlos.
		function listarGrupos($datos){
			return $this->db->select("SELECT * FROM grupo WHERE empresa = :idempresa",array("idempresa"=>$datos));
		}

		// Recoge los datos de un grupo en particular.
		function grupo($id){
			return $this->db->select("SELECT * FROM grupo WHERE idgrupo=:id",array("id"=>$id));
		}

		// Insertará un grupo en la base de datos.
		function registrarGrupo($datos){
			return $this->db->insert('grupo',$datos);
		}

		// Hace el Update de un grupo en particular.
		function actualizarGrupo($id,$datos){
			return $this->db->update("grupo",$datos,"idgrupo=".$id);
		}

		// Hace el select de las categorias de trabajadores para poder listarlos.
		function listarCategorias(){
			return $this->db->select("SELECT * FROM categoria");
		}

		// Recoge los datos de una categoria en particular.
		function categoria($id){
			return $this->db->select("SELECT * FROM categoria WHERE idCategoria=:id",array("id"=>$id));
		}

		// Hace el Update de un grupo en particular.
		function actualizarCategoria($id,$datos){
			return $this->db->update("categoria",$datos,"idCategoria=".$id);
		}

		// Hace el Update de la información de la empresa.
		function actualizarEmpresa($id,$datos){
			return $this->db->update("empresa",$datos,"idempresa=".$id);
		}

		// Hace el select de los roles de los trabajadores para poder listarlos.
		function listarRoles(){
			return $this->db->select("SELECT * FROM rol");
		}

		// Hace el select de los estados civiles de los trabajadores para poder listarlos.
		function listarEstados(){
			return $this->db->select("SELECT * FROM estado_civil");
		}
		
	}
?>