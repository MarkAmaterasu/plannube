<?php
// Define las distintas rutas del proyecto.
define('URL', 'http://localhost/plannube.es/'); // Ruta del DocumentRoot.
define('LIB', 'aplicacion/_librerias/'); // Ruta de las librerias.
define('CONTROLADOR', 'aplicacion/controladores/'); //	Ruta de los controladores.
define('VISTA', 'aplicacion/vistas/'); // Ruta de las vistas.
define('IDIOMA', 'aplicacion/idiomas/'); // Ruta de las traducciones a distintos idiomas.

// Define los parámetros para la conexión a la base de datos.
define('DB_HOST', 'localhost'); // Host de la base de datos.
define('DB_USER', 'root'); // Usuario de la base de datos.
define('DB_PASS', ''); // Contraseña de la base de datos.
define('DB_NAME', 'humanProject'); // Nombre de la base de datos.
define('DB_SYSTEM','mysql'); // Sistema de la base de datos.

// Define las constantes del cifrado de las contraseñas.
define('ALGORITMO', 'sha512');
define('LLAVE_CIFRADO', 'DAW2Project');

define("SECURE", FALSE);
?>