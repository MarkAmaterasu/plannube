<?php
/////////////////////////////////	GENERAL 	///////////////////////////////////////////////////
define("CONTRASENYA", "Password");
define("CONECTAR", "Connect");
define("CERRAR", "Close");
define("SERVICIOS", "Services");
define("CONTACTO", "Contact");
define("TELEFONO", "Phone");
define("NOMBRE", "Name");
define("MENSAJE", "Message");
define("INICIO", "Home");
define("TRABAJADORES", "Workers");
define("CERRARSESION", "Logout");
define("GRUPOS", "Groups");
define("CATEGORIAS", "Categories");
define("AÑADIRTRABAJADORES", "Add workers");
define("PLANOSDETRABAJO", "Work plans");
define("HOLA", "Hello, ");
define("EDITAR", "Edit");
define("ALTERTELF", "Alternate telephone");

/////////////////////////////////	INDEX	///////////////////////////////////////////////////////
define("INDEXERRORLOGIN", "Email or wrong password!");
define("INDEXENTRARLOGIN", "Login");
define("INDEXEJEMPLOEMAIL", "exemple@exemple.com");
define("INDEXQUIENESSOMOS", "About");
define("INDEXTEXTSOMOS", "<b>Plannube.es</b> is a company that provides a service ...");

/////////////////////////////////	PERFIL EMPRESA	///////////////////////////////////////////////////////
define("PERFILEFISCAL", "Fiscal name");
define("PERFILECOMERCIAL", "Business name");
define("PERFILEDIRECCION", "Fiscal address");

/////////////////////////////////	MENÚ SUPERIOR	///////////////////////////////////////////////////////
define("MENUIDIOMA", "Language");
?>