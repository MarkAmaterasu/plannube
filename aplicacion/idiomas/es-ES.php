<?php
/////////////////////////////////	GENERAL 	///////////////////////////////////////////////////
define("CONTRASENYA", "Contraseña");
define("CONECTAR", "Conectar");
define("CERRAR", "Cerrar");
define("SERVICIOS", "Servicios");
define("CONTACTO", "Contacto");
define("TELEFONO", "Teléfono");
define("NOMBRE", "Nombre");
define("MENSAJE", "Mensaje");
define("INICIO", "Inicio");
define("TRABAJADORES", "Trabajadores");
define("CERRARSESION", "Cerrar sesión");
define("GRUPOS", "Grupos");
define("CATEGORIAS", "Categorias");
define("AÑADIRTRABAJADORES", "Añadir trabajadores");
define("PLANOSDETRABAJO", "Planos de trabajo");
define("HOLA", "Hola, ");
define("EDITAR", "Editar");
define("ALTERTELF", "Teléfono alternativo");

/////////////////////////////////	INDEX	///////////////////////////////////////////////////////
define("INDEXERRORLOGIN", "¡Email o contraseña incorrecta!");
define("INDEXENTRARLOGIN", "Entrar");
define("INDEXEJEMPLOEMAIL", "ejemplo@ejemplo.com");
define("INDEXQUIENESSOMOS", "Quiénes somos");
define("INDEXTEXTSOMOS", "<b>Plannube.es</b> es una empresa que ofrece un servicio...");

/////////////////////////////////	PERFIL EMPRESA	///////////////////////////////////////////////////////
define("PERFILEFISCAL", "Nombre fiscal");
define("PERFILECOMERCIAL", "Nombre comercial");
define("PERFILEDIRECCION", "Dirección fiscal");

/////////////////////////////////	MENÚ SUPERIOR	///////////////////////////////////////////////////////
define("MENUIDIOMA", "Idioma");
?>