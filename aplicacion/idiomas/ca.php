<?php
/////////////////////////////////	GENERAL 	///////////////////////////////////////////////////
define("CONTRASENYA", "Contrasenya");
define("CONECTAR", "Connectar");
define("CERRAR", "Tancar");
define("SERVICIOS", "Serveis");
define("CONTACTO", "Contacte");
define("TELEFONO", "Telèfon");
define("NOMBRE", "Nom");
define("MENSAJE", "Missatge");
define("INICIO", "Inici");
define("TRABAJADORES", "Treballadors");
define("CERRARSESION", "Tancar sessió");
define("GRUPOS", "Grups");
define("CATEGORIAS", "Categories");
define("AÑADIRTRABAJADORES", "Afegir treballadors");
define("PLANOSDETRABAJO", "Planòl de treball");
define("HOLA", "Hola, ");
define("EDITAR", "Editar");
define("ALTERTELF", "Telèfon alternatiu");

/////////////////////////////////	INDEX	///////////////////////////////////////////////////////
define("INDEXERRORLOGIN", "Email o contrasenya incorrecte!");
define("INDEXENTRARLOGIN", "Entrar");
define("INDEXEJEMPLOEMAIL", "exemple@exemple.com");
define("INDEXQUIENESSOMOS", "Qui som");
define("INDEXTEXTSOMOS", "<b>Plannube.es</b> és una empresa que ofereix un servei...");

/////////////////////////////////	PERFIL EMPRESA	///////////////////////////////////////////////////////
define("PERFILEFISCAL", "Nom fiscal");
define("PERFILECOMERCIAL", "Nom comercial");
define("PERFILEDIRECCION", "Direcció fiscal");

/////////////////////////////////	MENÚ SUPERIOR	///////////////////////////////////////////////////////
define("MENUIDIOMA", "Idioma");

?>