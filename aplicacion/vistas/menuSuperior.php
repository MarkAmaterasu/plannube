<!-- Menú Superior para el rol 1 (Empresa) -->
<?php if (Sesion::getValue('ROL') == 1): ?>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Para una mejor visualización en dispositivos móviles, agrupa los enlaces en un botón. -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/perfil"><?php echo $empresa['nombre_comercial']; ?></a>
        </div>

        <!-- Recoge los enlaces de navegación, formularios y otros contenidos para añadir al botón toggle. -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/perfil"><?php echo INICIO ?><span class="sr-only"><?php echo INICIO ?></span></a></li>
                <li><a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/listado"><?php echo TRABAJADORES ?></a></li>
                <li role="separator" class="nav-divider"></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo MENUIDIOMA ?><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="center-block">
                            <a href="<?php echo URL; ?>ca/Empresa/perfil">
                                <img src="<?php echo URL; ?>assets/img/idiomas/catalunya.png" alt="Bandera de Catalunya" class="img-responsive imgIdiomaNav">
                                <span class="label sr-only">Català</span>
                            </a>
                        </li>
                        <li class="">                 
                            <a href="<?php echo URL; ?>es/Empresa/perfil">
                                <img src="<?php echo URL; ?>assets/img/idiomas/es.png" alt="Bandera española" class="img-responsive imgIdiomaNav">
                                <span class="label sr-only">Castellano</span>
                            </a>                                 
                        </li>
                        <li class=""> 
                            <a href="<?php echo URL; ?>en/Empresa/perfil">
                                <img src="<?php echo URL; ?>assets/img/idiomas/ingles.png" alt="Bandera inglesa" class="img-responsive imgIdiomaNav">
                                <span class="label sr-only">English</span>
                            </a>                                
                        </li>
                    </ul>
                </li>

                <li><a id="botonCerrarSesion" href="#"><?php echo CERRARSESION ?></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<!-- Menú Superior para el rol 2 (Admin) -->
<?php elseif(Sesion::getValue('ROL') == 2): ?>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Para una mejor visualización en dispositivos móviles, agrupa los enlaces en un boton. -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/perfil"><?php echo $empresa['nombre_comercial']; ?></a>
        </div>

        <!-- Recoge los enlaces de navegación, formularios y otros contenidos para añadir al botón toggle. -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/perfil"><?php echo INICIO ?><span class="sr-only"><?php echo INICIO ?></span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo URL; ?>ca/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/catalunya.png" alt="Bandera de Catalunya" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">Català</span>
                    </a>
                </li>
                <li class="">                 
                    <a href="<?php echo URL; ?>es/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/es.png" alt="Bandera española" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">Castellano</span>
                    </a>                                 
                </li>
                <li class=""> 
                    <a href="<?php echo URL; ?>en/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/ingles.png" alt="Bandera inglesa" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">English</span>
                    </a>                                
                </li>
                <li><a id="botonCerrarSesion" href="#"><?php echo CERRARSESION ?></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!-- Menú superior para el rol 3 (Trabajador) -->
<?php  elseif(Sesion::getValue('ROL') == 3): ?>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Para una mejor visualización en dispositivos móviles, agrupa los enlaces en un boton. -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/perfil"><?php echo $empresa['nombre_comercial']; ?></a>
        </div>

        <!-- Recoge los enlaces de navegación, formularios y otros contenidos para añadir al botón toggle. -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo URL.$GLOBALS['language']; ?>/Trabajador/perfil"><?php echo INICIO ?></a></li>
                <li><a href="<?php echo URL.$GLOBALS['language']; ?>/Trabajador/horario"><?php echo HORARIO ?></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo URL; ?>ca/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/catalunya.png" alt="Bandera de Catalunya" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">Català</span>
                    </a>
                </li>
                <li class="">                 
                    <a href="<?php echo URL; ?>es/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/es.png" alt="Bandera española" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">Castellano</span>
                    </a>                                 
                </li>
                <li class=""> 
                    <a href="<?php echo URL; ?>en/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/ingles.png" alt="Bandera inglesa" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">English</span>
                    </a>                                
                </li>
                <li><a id="botonCerrarSesion" href="#"><?php echo CERRARSESION ?></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?php else:?>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Para una mejor visualización en dispositivos móviles, agrupa los enlaces en un boton. -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo URL.$GLOBALS['language']; ?>/Index/index">HumanProject</a>
        </div>

        <!-- Recoge los enlaces de navegación, formularios y otros contenidos para añadir al botón toggle. -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo URL.$GLOBALS['language']; ?>/Index/index"><?php echo INICIO ?></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo URL; ?>ca/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/catalunya.png" alt="Bandera de Catalunya" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">Català</span>
                    </a>
                </li>
                <li class="">                 
                    <a href="<?php echo URL; ?>es/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/es.png" alt="Bandera española" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">Castellano</span>
                    </a>                                 
                </li>
                <li class=""> 
                    <a href="<?php echo URL; ?>en/Empresa/perfil">
                        <img src="<?php echo URL; ?>assets/img/idiomas/ingles.png" alt="Bandera inglesa" class="img-responsive imgIdiomaNav">
                        <span class="label sr-only">English</span>
                    </a>                                
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?php endif; ?>
