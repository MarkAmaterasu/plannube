<?php 
	include ('aplicacion/vistas/header.php');
?>
<header>
	<a href="<?php echo URL; ?>es/Index/index"> Castellano </a>
	<a href="<?php echo URL; ?>ca/Index/index"> Catalán </a>
	<a href="<?php echo URL; ?>en/Index/index"> Inglés </a>
</header>
<body>
	 <!-- Menú público -->
        <a href="<?php echo URL.$GLOBALS['language']; ?>/Index/index">Inicio</a>
        <a href="">Human Project</a>
        <a href="<?php echo URL.$GLOBALS['language']; ?>/Index/contacto">Contacto</a>
    <!-- Fin menú público -->
    Esta es la vista de la página de contacto!

    <form action="<?php echo URL.$GLOBALS['language']; ?>/Index/contacto" method="post" name="formContacto">
		<label for="nombre">Nombre: </label>
		<input id="nombre" type="text" name="nombre" placeholder="Nombre y Apellido" required="" />
		<label for="email">Email: </label>
		<input id="email" type="email" name="email" placeholder="ejemplo@correo.com" required="" />
		<label for="mensaje">Mensaje:</label>
		<textarea id="mensaje" name="mensaje" placeholder="Mensaje" required="" rows=8 cols=45></textarea>
		<input id="contacto" type="submit" name="contacto" value="Enviar" />
	</form>

	<!-- Script de envio del formulario de contacto mediante Ajax/Jquery -->
	<script>
        $(document).ready(function(){                     
                            
            // Al hacer click al boton siguiente, ejecuta la función definida dentro de ella.                     
            $('#contacto').click(function(e){      
                e.preventDefault();
                contacto();
            });

            // Función que se encarga, mediante ajax, del envio del mensaje.
            function contacto(){
                        
                var nombre = $('form[name=formContacto] input[name=nombre]')[0].value;      
                var email = $('form[name=formContacto] input[name=email]')[0].value;
                var mensaje = $('form[name=formContacto] textarea[name=mensaje]')[0].value;
                        
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL.$GLOBALS['language']; ?>/Index/envioContacto",
                    data: 
                        {   
                            nombre: nombre, 
                            email: email, 
                            mensaje: mensaje
                        }
                })

                .done(function(){
                    alert("¡Envio efectuado con éxito!");  
                })

                .fail(function(){
                    alert("¡Error en el envio!");
                })                        
            }
        });
	</script>
</body>
</html>