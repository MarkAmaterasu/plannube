<?php
	include ('aplicacion/vistas/header.php');
?>
    <body>
        <!-- Si no existe una sesión, mostrará lo siguiente. -->
        <?php if(!Sesion::existe()){ ?>
        <div class="container-fluid">
           
            <!-- Fila que ocupa el lateral izquierda de la vista -->
            <div class="row lateral">
                <div class="row">
                    <div class="col-sm-5 col-md-4 col-lg-3">
                        <!--Imagen del logotipo de la empresa-->
                        <img src="<?php echo URL; ?>assets/img/logo.ico" alt="Logotipo" class="img-responsive img-circle hidden-xs hidden-sm" id="logotipo">
                    </div>
                    <div class="pull-left col-xs-12 col-sm-12 col-md-7 col-lg-9 posLateral">
                        <h1 class="titulosLaterales">Plannube</h1>
                    </div>
                </div>

                <!-- Botón solo visible para móbiles, abre un modal con el formulario de acceso.-->
                <div class="row visible-xs" id="colBotonLogin">
                    <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalLogin"><?php echo CONECTAR ?></button>
                </div>

                <!-- Título y formulario de acceso a la aplicación. No visible desde móbil. -->
                <div class="row hidden-xs" id="entrar">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 posLateral">
                        <h2 class="titulosLaterales"><?php echo INDEXENTRARLOGIN ?></h2>
                    </div>
                </div>

                <div class="row">                   
                    <!--                   Formulario de acceso a la aplicación.                    -->
                    <form action="<?php URL.$GLOBALS['language']; ?>/Empresa/entrar" name="formLogin" method="post" class="formLogin form-horizontal hidden-xs">
                        <div class="form-group ">
                            <label for="email" class="sr-only col-lg-2 control-label"> Email </label>
                            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon" ><b>@</b></span>
                                    <input type="text" class="form-control" placeholder="<?php echo INDEXEJEMPLOEMAIL ?>" name="email" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contrasenya" class="sr-only col-lg-2 control-label"> <?php echo CONTRASENYA ?> </label>
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon glyphicon glyphicon-log-in"></span>
                                    <input type="password" class="form-control" placeholder="<?php echo CONTRASENYA ?>" name="contrasenya" required> 
                                </div>
                            </div>                             
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-7">
                                <button name="botonLogin" type="submit" class="botonLogin btn btn-default"><?php echo CONECTAR ?></button>
                            </div>
                        </div>
                    </form>                    
                </div>
            </div>
            <!--           Fila que contiene los botones para cambiar de idioma -->
            <div class="row">                    
                <div class="idioma col-xs-6 col-sm-7 col-md-7 col-lg-6 col-xs-offset-6 col-sm-offset-5 col-md-offset-5 col-lg-offset-5  text-center">
                    <ul class="list-inline">
                        <li class="">                 
                            <a href="<?php echo URL; ?>ca/Index/index">
                                <img src="<?php echo URL; ?>assets/img/idiomas/catalunya.png" alt="Bandera de Catalunya" class="img-responsive imgIdioma">
                                <span class="label sr-only">Català</span>
                            </a>                              
                        </li>
                        <li class="">                 
                            <a href="<?php echo URL; ?>es/Index/index">
                                <img src="<?php echo URL; ?>assets/img/idiomas/es.png" alt="Bandera española" class="img-responsive imgIdioma">
                                <span class="label sr-only">Castellano</span>
                            </a>                                 
                        </li>
                        <li class=""> 
                            <a href="<?php echo URL; ?>en/Index/index">
                                <img src="<?php echo URL; ?>assets/img/idiomas/ingles.png" alt="Bandera inglesa" class="img-responsive imgIdioma">
                                <span class="label sr-only">English</span>
                            </a>                                
                        </li>
                    </ul>
                </div>
            </div>
            <!--            Div principal para el contenido. Situado a la derecha de la vista.-->
            <div class="principal">


                <!--           Fila donde se explica quiénes somos. -->
                <div class="row">                    
                    <div class="col-xs-8 col-sm-7 col-md-7 col-lg-6 col-xs-offset-4 col-sm-offset-5 col-md-offset-5 col-lg-offset-5  text-center">
                        <h1 class="contenido"><?php echo INDEXQUIENESSOMOS ?></h1>
                        <p class="lead contenido"><?php echo INDEXTEXTSOMOS ?></p>
                    </div>
                </div>

                <!--                Fila donde se explica los servicios que ofrecemos. -->
                <div class="row">
                    <div class="col-xs-8 col-sm-7 col-md-7 col-lg-6 col-xs-offset-4 col-sm-offset-5 col-md-offset-5 col-lg-offset-5  text-center">
                        <h1 class="contenido"><?php echo SERVICIOS ?></h1>
                        <div class="lead contenido"><dl><dt>asd</dt><dd>asd2</dd> <dt>asd3</dt><dd>asd4</dd> </dl></div>
                    </div>
                </div>

                <!--                Fila donde se situa el formulario de contacto. -->
                <div class="row">
                    <div class="col-xs-8 col-sm-7 col-md-7 col-lg-6 col-xs-offset-4 col-sm-offset-5 col-md-offset-5 col-lg-offset-5  text-center">
                       <div class="row">
                        <h1 class="contenido"><?php echo CONTACTO ?></h1>
                            <div class="lead contenido col-md-11 col-lg-9" id="divFormContacto">
                                <form action="<?php echo URL.$GLOBALS['language']; ?>/Index/contacto" method="post" name="formContacto" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="nombre" class="fcontacto contenido control-label col-lg-2"><?php echo NOMBRE ?>*</label>
                                        <div class="col-lg-5">                                       
                                                <input class="form-control input-sm" id="nombre" type="text" name="nombre" required />                                        
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="fcontacto contenido control-label col-lg-2">Email*</label>  
                                        <div class="col-lg-5">
                                            <input class="form-control input-sm" id="email" type="email" name="email" required />
                                        </div>  
                                    </div>
                                    <div class="form-group">
                                        <label for="mensaje" class="fcontacto contenido control-label col-lg-2"><?php echo MENSAJE ?>*</label>
                                        <div class="col-lg-5">
                                            <textarea class="form-control input-sm" id="mensaje" name="mensaje" required rows=8 cols=45></textarea>
                                        </div>  
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-11 col-lg-8">
                                            <input class="btn btn-default" id="contacto" type="submit" name="contacto" value="Enviar" />    
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="contenido col-lg-3" id="infoContacto">
                              <dl>
                                  <dt><u><?php echo TELEFONO ?> </u></dt>
                                  <dd>123456789</dd>
                                  <dt><u>Email</u></dt>
                                  <dd>emilio@emilio.com</dd>
                                  <br>
                                  <dt>Plannube.SL</dt>
                                  <dt>c/ Falsa, 123</dt>
                                  <br>
                                  <dt>1234 cerebro (Mi cabeza) </dt>
                                  <dt>Felizonia</dt>
                              </dl>                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Inicio del modal que contiene el formulario de acceso a la aplicación desde móvil. -->
        <div class="modal fade" id="modalLogin" role="dialog">
            <div class="modal-dialog">

                <!-- Contenido del Modal -->
                <div class="modal-content">

                    <!-- Botón de cierre del modal-->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php echo INDEXENTRARLOGIN ?></h4>
                    </div>

                    <!-- Formulario de acceso para móvil-->
                    <div class="modal-body">
                        <form action="<?php URL.$GLOBALS['language']; ?>/Empresa/entrar" name="formLogin" method="post" class="formLogin form-horizontal">
                            <div class="form-group ">
                                <label for="email" class="sr-only col-lg-2 control-label"> Email </label>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon" ><b>@</b></span>
                                        <input type="text" class="form-control" placeholder="<?php echo INDEXEJEMPLOEMAIL ?>" name="email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contrasenya" class="sr-only col-lg-2 control-label"> <?php echo CONTRASENYA ?> </label>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon glyphicon glyphicon-log-in"></span>
                                        <input type="password" class="form-control" placeholder="<?php echo CONTRASENYA ?>" name="contrasenya" required> 
                                    </div>
                                </div>                             
                            </div>
                            <div class="form-group ">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-7">
                                    <button name="botonLogin" type="submit" class="botonLogin btn btn-default"><?php echo CONECTAR ?></button>
                                </div>
                            </div>
                        </form> 
                    </div>

                    <!--                    Botón para cerrar el modal-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo CERRAR ?></button>
                    </div>
                </div>
            </div>
        </div>

        <!--        Footer de la página.-->
        <footer class="footerIndex">
            <div class="col-xs-12 col-sm-9 col-md-4 col-lg-4 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                <!--               Lista con las imágenes de las redes sociales.-->
                <ul class="social list-inline">
                    <li>
                        <a href="">
                            <img src="<?php echo URL; ?>assets/img/socialIcons/64/Twitter.png" alt="Imagen Twitter" class="imgSocial">
                            <span class="label sr-only">Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="<?php echo URL; ?>assets/img/socialIcons/64/Facebook.png" alt="Imagen Facebook" class="imgSocial">
                            <span class="label sr-only">Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="<?php echo URL; ?>assets/img/socialIcons/64/linkedin.png" alt="Imagen LinkedIn" class="imgSocial">
                            <span class="label sr-only">LinkedIn</span>
                        </a>
                    </li>
                </ul>
                <ul class="copyright">
                    <li> Copyright &copy; 2015 Plannube</li>
                    <li>
                        <span class="glyphicon glyphicon-envelope"></span> 
                        <a href="mailto:emilio@emilio.com">emilio@emilio.com</a>
                    </li>
                </ul>
            </div>
        </footer>
        <!-- Por lo contrario, si existe una sesión, mostrará esto otro.-->
        <?php }else{ ?>

            <div class="formWrapper">
                <?php echo Sesion::getValue('NOMBREUSUARIO'); ?>
                <a id="botonCerrarSesion" href="#">Cerrar Sesión</a>
            </div>
         
        <?php } ?>

        <script>
            $(document).ready(function(){
                                
                // Al hacer click al botones siguientes, ejecutan la función definida dentro de ella.    
                $('.botonLogin').click(function(e){
                    // Evita la acción por defecto del evento en cuestión.
                    e.preventDefault();
                    login();
                });
                                    
                $('#contacto').click(function(e){      
                    e.preventDefault();
                    contacto();
                });

                // Función que se encarga, mediante ajax, del login de usuarios.
                function login(){

                    var email = $('form[name=formLogin] input[name=email]')[0].value;
                    var contrasenya = $('form[name=formLogin] input[name=contrasenya]')[0].value;

                    // En el caso de que el primer formulario esté vacío, cogerá los datos introducidos del segundo.
                    if (email === "" && contrasenya === "") {
                        email = $('form[name=formLogin] input[name=email]')[1].value;
                        contrasenya = $('form[name=formLogin] input[name=contrasenya]')[1].value;
                    };

                    $.ajax({
                        type: "POST",
                        url: "<?php echo URL.$GLOBALS['language']; ?>/Empresa/entrar",
                        data: 
                            {
                                email: email,
                                contrasenya: contrasenya
                            }
                    })

                    .done(function(respuesta){
                        if(respuesta == 1){
                            location.reload();
                        }else{
                            alert("<?php echo  INDEXERRORLOGIN ?>");
                        }
                    });
                }
                
                // Función que se encarga, mediante ajax, del envio del mensaje.
                function contacto(){

                    var nombre = $('form[name=formContacto] input[name=nombre]')[0].value;      
                    var email = $('form[name=formContacto] input[name=email]')[0].value;
                    var mensaje = $('form[name=formContacto] textarea[name=mensaje]')[0].value;

                    $.ajax({
                        type: "POST",
                        url: "<?php echo URL.$GLOBALS['language']; ?>/Index/envioContacto",
                        data: 
                            {   
                                nombre: nombre, 
                                email: email, 
                                mensaje: mensaje
                            }
                    })

                    .done(function(){
                        alert("¡Envio efectuado con éxito!");  
                    })

                    .fail(function(){
                        alert("¡Error en el envio!");
                    })                        
                }
            });
        </script>
    </body>
</html>