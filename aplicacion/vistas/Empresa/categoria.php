<?php
	include ('aplicacion/vistas/header.php');
?>
 <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1): ?>
    <button id="actualizar">Editar</button>
    <?php
      // Recupera las categorias desde la base de datos.
      $categoria = $this->categoria;
      // Crea una tabla con la información de la categoria anteriormente recuperada.
      echo <<< EOT
          <table>
            <tr> 
              <td></td>
              <td>NOMBRE</td>
            </tr>
EOT;


        echo <<< EOT

            <tr>
              <td></td>
              <td>$categoria[nombre]</td>
            </tr>          

EOT;

    echo "</table>";
    ?>
  </div>
           <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>         
        
<script>
  $(function(){
    $('#botonCerrarSesion').click(function(){
      document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
    });

    $('#actualizar').click(function(){
      document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/actualizarCategoria/<?php echo $categoria['idCategoria'] ?>";
    });
  });
</script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>