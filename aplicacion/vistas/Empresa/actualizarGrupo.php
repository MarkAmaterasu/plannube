<?php
	include ('aplicacion/vistas/header.php');
?>
<body>
  <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1): ?>
    Hola, esta és la página donde se actualizarán los datos de los trabajadores! <?php echo EJEMPLO ?><br>
    <?php 
      // Recupera los trabajadores desde la base de datos.
      $grupo = $this->grupo;
    ?>

    <div class="formWrapper">
            <?php  echo "Estás conectado como ".Sesion::getValue('NOMBREUSUARIO'); ?>
            <!-- Menú superior -->
          <?php
          include ('aplicacion/vistas/menuSuperior.php');
          // Menú lateral.
          include ('aplicacion/vistas/menuLateral.php');
          ?>
        </div>
        <div>
          <button id="guardarCambios">Guardar cambios</button>
          <button id="cancelar">Cancelar</button>
          <!-- Formulario actualización del grupo -->
          <form action="" method="POST" name="actualizar" id="actualizar">
            <input type="text" id="nombre" name="nombre" value="<?php echo  $grupo['nombreGrupo'] ?>" placeholder="Nombre"> 
          </form>
          <!-- Fin formulario -->
        </div>
        
        <script>
            $(function(){
              // Función Jquery que destruirá la sesión llamando al método destruirSesion
              $('#botonCerrarSesion').click(function(){
                document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
              });

              // Al hacer click a "Cancelar", nos redigirá a la página anterior.
              $('#cancelar').click(function(e) {
                document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/Grupo/<?php echo $grupo['idgrupo'] ?>";
              });

              // Al hacer click al botón "Guardar cambios", ejecutará la función actualizar.
              $('#guardarCambios').click(function(e){      
                e.preventDefault();
                actualizar(<?php echo $grupo['idgrupo'] ?>);
              });

              // Función que se encarga, mediante ajax, de actualizar los datos de un trabajador.
              function actualizar(id){
                // var = (comparación) ? verdadero : falso;    
                var nombre = $('form[name=actualizar] input[name=nombre]')[0].value;

                nombre = (nombre != "<?php echo $grupo['nombreGrupo'] ?>" && nombre != '') ? nombre : "<?php echo $grupo['nombreGrupo'] ?>";
                        
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL.$GLOBALS['language'];?>/Empresa/updateGrupo",
                    data: 
                        {   
                            id : id,
                            nombre: nombre
                        }
                })

                .done(function(){
                    alert("¡Actualización efectuada con éxito!");
                    document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/grupo/<?php echo $grupo['idgrupo'] ?> ";
                })

                .fail(function(){
                    alert("¡Error en la actualización!");
                })                        
              }
            });
        </script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>