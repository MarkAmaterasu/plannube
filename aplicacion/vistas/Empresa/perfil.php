<?php
    // Incluimos el Header a la vista.
	include ('aplicacion/vistas/header.php');
?>
  <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1):?>
              <div class="row">
                 <div  class="col-lg-3 col-lg-offset-12 pull-right botonesEdit">
                     <button id="actualizar" name="actualizar" class="btn btn-default"><?php echo EDITAR ?></button>
                 </div>                  
              </div>               
                <?php
                      // Crea una tabla con la información de la empresa.
                      echo'
                          <table class="table">
                            <tr> 
                              <td><b>'.PERFILEFISCAL.'</b></td>
                              <td>'.$empresa["nombre_fiscal"].'</td>
                            </tr>
                            <tr>
                              <td><b>'.PERFILECOMERCIAL.'</b></td>
                              <td>'.$empresa["nombre_comercial"].'</td>
                            </tr>
                            <tr>
                              <td><b>CIF</b></td>
                              <td>'.$empresa["cif"].'</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td>'.$empresa["email"].'</td>
                            </tr>
                            <tr>
                              <td><b>'.TELEFONO.'</b></td>
                              <td>'.$empresa["telefono"].'</td>
                            </tr>
                            <tr>
                              <td><b>'.ALTERTELF.'</b></td>
                              <td>'.$empresa["telefono_alternativo"].'</td>
                            </tr>
                            <tr>
                              <td><b>'.CONTACTO.'</b></td>
                              <td>'.$empresa["contacto"].'</td>
                            </tr>
                            <tr>
                              <td><b>'.PERFILEDIRECCION.'</b></td>
                              <td>'.$empresa["direccion_fiscal"].'</td>
                            </tr>
                          </table>'
                ?>
           </div>
           <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>        
        <script>
            $(function(){
               $('#botonCerrarSesion').click(function(){
                  document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
               });
               $('#actualizar').click(function(){
                  document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/actualizarEmpresa";
               });
            });
        </script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>