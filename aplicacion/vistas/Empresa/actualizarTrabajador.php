<?php
	include ('aplicacion/vistas/header.php');
?>
  <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1): ?>
    <?php 
      // Recupera los trabajadores desde la base de datos.
      $trabajador = $this->trabajador;
      // Recupera los grupos desde la base de datos.
      $grupos = $this->listarGrupos;
      // Recupera los roles de la base de datos.
      $roles = $this->listarRoles;
      // Recupera los diferentes estados civiles de la base de datos.
      $estadosCiviles = $this->listarEstados;
    ?>
          <button id="guardarCambios">Guardar cambios</button>
          <button id="cancelar">Cancelar</button>
          <!-- Formulario actualización de trabajadores -->
          <form action="" method="POST" name="actualizar">
            <input type="text" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo  $trabajador['nombre'] ?>">
            <input type="text" placeholder="Primer apellido" value="<?php echo $trabajador['apellido1'] ?>">
            <input type="text" placeholder="Segundo apellido" value="<?php echo $trabajador['apellido2'] ?>">
            <input type="text" placeholder="DNI" value="<?php echo $trabajador['dni'] ?>">
            <input type="text" placeholder="NIE">
            <input type="text" placeholder="Pasaporte">
            <input type="text" placeholder="Dirección">
            <input type="text" placeholder="Código postal">
            <input type="text" placeholder="Municipio">
            <input type="text" placeholder="Provincia">
            <input type="text" placeholder="Pais">
            <input type="text" placeholder="Teléfono fijo">
            <input type="text" placeholder="Teléfono móvil">
            <input type="text" placeholder="Teléfono empresa">
            <input type="text" id="email" name="email" placeholder="Correo electrónico" value="<?php echo $trabajador['email'] ?>">
            <input type="text" placeholder="Cuenta bancaria">
            ¿Hijos?: <select>
              <option></option>
              <option> Sí </option>
              <option> No </option>
            </select>
            <input type="text" placeholder="Número de hijos">
            Estado civil :<select> 
              <option></option>
              <option>Soltero</option>
              <option>Casado</option>
            </select>
            <input type="text" placeholder="Número seguridad social">
            <input type="text" placeholder="Horas semanales">
            <input type="text" placeholder="Duración del contrato">
            <input type="text" placeholder="Tipo de contrato">
            <input type="text" placeholder="Tiempo de prueba">
            <input type="text" placeholder="Categoria salarial">
            <input type="text" placeholder="Previsión fin de contrato">
            <input type="text" placeholder="Departamento">
            <input type="text" placeholder="Cantidad plus">
            <input type="text" placeholder="jefe">
            Baja <select>
              <option></option>
              <option value="">Si</option>
              <option value="">No</option>
            </select>
            Tipo  de baja: <select> 
              <option value=""></option>
              <option value="">General</option>
              <option value="">Laboral</option>
              <option value="">Maternidad / Paternidad</option>
            </select>
            <input type="text" placeholder="Dias semanales">
            Rol:
            <select name="rol" id="rol">
              <option value="<?php echo $trabajador['nombreRol'] ?>"><?php echo $trabajador['nombreRol'] ?></option>
            <?php
            foreach ($roles as $rol) {
              echo <<< EOT
              <option value="$rol[idrol]">$rol[nombreRol]</option>
EOT;
             } 
?>
            </select>
            Grupo:
            <select name="grupo" id="grupo">
            <option value="<?php echo $trabajador['nombreGrupo'] ?>"><?php echo $trabajador['nombreGrupo'] ?></option> 
            <?php
            foreach ($grupos as $grupo) {
              echo <<< EOT
              <option value="$grupo[idgrupo]">$grupo[nombreGrupo]</option>
EOT;
            }
?>              
            </select>
          </form>
          <!-- Fin formulario -->
        </div>
                   <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>  
        
        <script>
            $(function(){
                // Función Jquery que destruirá la sesión llamando al método destruirSesion
               $('#botonCerrarSesion').click(function(){
                  document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
               });
              // Al hacer click al botón "Guardar cambios", ejecutará la función actualizar.
              $('#guardarCambios').click(function(e){      
                e.preventDefault();
                actualizar(<?php echo $trabajador['idtrabajador'] ?>);
              });

              // Al hacer click a "Cancelar", nos redigirá a la página anterior.
              $('#cancelar').click(function(e) {
                document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/Trabajador/<?php echo $trabajador['idtrabajador'] ?>";
              });

              // Función que se encarga, mediante ajax, de actualizar los datos de un trabajador.
              function actualizar(id){
                // var = (comparación) ? verdadero : falso;    
                var nombre = $('form[name=actualizar] input[name=nombre]')[0].value;

                nombre = (nombre != "<?php echo $trabajador['nombre'] ?>" && nombre != '') ? nombre : "<?php echo $trabajador['nombre'] ?>";

                var email = $('form[name=actualizar] input[name=email]')[0].value;
                        
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL.$GLOBALS['language'];?>/Empresa/updateTrabajador",
                    data: 
                        {   
                            id : id,
                            nombre: nombre,  
                            email: email
                        }
                })

                .done(function(){
                    alert("¡Actualización efectuada con éxito!");
                    document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/trabajador/<?php echo $trabajador['idtrabajador'] ?> ";
                })

                .fail(function(){
                    alert("¡Error en la actualización!");
                })
                        
            }

            });
        </script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>