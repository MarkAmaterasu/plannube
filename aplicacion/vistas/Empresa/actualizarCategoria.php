<?php
	include ('aplicacion/vistas/header.php');
?>
  <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1): ?>
    <?php 
      // Recupera las categorias desde la base de datos.
      $categoria = $this->categoria;
    ?>
          <button id="guardarCambios">Guardar cambios</button>
          <button id="cancelar">Cancelar</button>
          <!-- Formulario actualización de la categoria -->
          <form action="" method="POST" name="actualizar">
            <input type="text" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo  $categoria['nombre'] ?>">            
          </form>
          <!-- Fin formulario -->
        </div>
                   <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>  
        
        <script>
            $(function(){
                // Función Jquery que destruirá la sesión llamando al método destruirSesion
               $('#botonCerrarSesion').click(function(){
                  document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
               });
              // Al hacer click al botón "Guardar cambios", ejecutará la función actualizar.
              $('#guardarCambios').click(function(e){      
                e.preventDefault();
                actualizar(<?php echo $categoria['idCategoria'] ?>);
              });

              // Función que se encarga, mediante ajax, de actualizar los datos de una categoria.
              function actualizar(id){
                // var = (comparación) ? verdadero : falso;    
                var nombre = $('form[name=actualizar] input[name=nombre]')[0].value;

                nombre = (nombre != "<?php echo $categoria['nombre'] ?>" && nombre != '') ? nombre : "<?php echo $categoria['nombre'] ?>";
                        
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL.$GLOBALS['language'];?>/Empresa/updateCategoria",
                    data: 
                        {   
                            id : id,
                            nombre: nombre
                        }
                })

                .done(function(){
                    alert("¡Actualización efectuada con éxito!");
                    document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/categoria/<?php echo $categoria['idCategoria'] ?> ";
                })

                .fail(function(){
                    alert("¡Error en la actualización!");
                })
                        
            }

            });
        </script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>