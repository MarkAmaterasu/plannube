<?php
include ('aplicacion/vistas/header.php');
?>

<!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
<?php if (Sesion::getValue('ROL') == 1): ?>
<div class="botonesEdit pull-right">
    <button class="btn btn-default" id="guardarCambios">Guardar cambios</button>
    <button class="btn btn-default" id="cancelar">Cancelar</button>
</div>

<!-- Formulario de actualización de los datos de la empresa -->
<form action="" method="POST" name="actualizar" class="form-horizontal">
    <?php
echo"
          <table class='table'>
            <tr> 
              <td><b>".PERFILEFISCAL."</b></td>
              <td><input class='form-control' value='$empresa[nombre_fiscal]' id='nombreFiscal' name='nombreFiscal'></td>
            </tr>
            <tr>
              <td><b>".PERFILECOMERCIAL."</b></td>
              <td><input class='form-control' value='$empresa[nombre_comercial]' id='nombreComercial' name='nombreComercial'></td>
            </tr>
            <tr>
              <td><b>CIF</b></td>
              <td><input class='form-control' value='$empresa[cif]' id='cif' name='cif'></td>
            </tr>
            <tr>
              <td><b>Email</b></td>
              <td><input class='form-control' value='$empresa[email]' id='email' name='email'></td>
            </tr>
            <tr>
              <td><b>".TELEFONO."</b></td>
              <td><input class='form-control' value='$empresa[telefono]' id='telefono' name='telefono'></td>
            </tr>
            <tr>
              <td><b>".ALTERTELF."</b></td>
              <td><input class='form-control' value='$empresa[telefono_alternativo]' id='telefonoAlter' name='telefonoAlter'></td>
            </tr>
            <tr>
              <td><b>".CONTACTO."</b></td>
              <td><input class='form-control' value='$empresa[contacto]' id='contacto' name='contacto'></td>
            </tr>
            <tr>
              <td><b>".PERFILEDIRECCION."</b></td>
              <td><input class='form-control' value='$empresa[direccion_fiscal]' id='direccionFiscal' name='direccionFiscal'></td>
            </tr>
          </table>"
    ?>
</form>
<!-- Fin formulario -->
</div>
<!-- Nav Derecha -->
<?php
    // Menú lateral.
    include ('aplicacion/vistas/menuLateral.php');
?>
</div>
</div>
<?php
// Incluimos el footer a la vista.
include("aplicacion/vistas/footer.php"); 
?>  

<script>
    $(function(){
        // Función Jquery que destruirá la sesión llamando al método destruirSesion
        $('#botonCerrarSesion').click(function(){
            document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
        });
        // Al hacer click al botón "Guardar cambios", ejecutará la función actualizar.
        $('#guardarCambios').click(function(e){      
            e.preventDefault();
            actualizar(<?php echo $empresa['idempresa'] ?>);
        });

        // Al hacer click a "Cancelar", nos redigirá a la página anterior.
        $('#cancelar').click(function(e) {
            document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/Perfil";
        });

        // Función que se encarga, mediante ajax, de actualizar los datos de un trabajador.
        function actualizar(id){
            // var = (comparación) ? verdadero : falso;    
            var nombreFiscal = $('form[name=actualizar] input[name=nombreFiscal]')[0].value;

            nombreFiscal = (nombreFiscal != "<?php echo $empresa['nombre_fiscal'] ?>" && nombreFiscal != '') ? nombreFiscal : "<?php echo $empresa['nombre_fiscal'] ?>";

            var email = $('form[name=actualizar] input[name=email]')[0].value;

            email = (email != "<?php echo $empresa['email'] ?>" && email != '') ? email : "<?php echo $empresa['email'] ?>";

            var nombreComercial = $('form[name=actualizar] input[name=nombreComercial]')[0].value;

            nombreComercial = (nombreComercial != "<?php echo $empresa['nombre_comercial'] ?>" && nombreComercial != '') ? nombreComercial : "<?php echo $empresa['nombre_comercial'] ?>";

            var telefono = $('form[name=actualizar] input[name=telefono]')[0].value;

            telefono = (telefono != "<?php echo $empresa['telefono'] ?>" && telefono != '') ? telefono : "<?php echo $empresa['telefono'] ?>";

            var telefonoAlter = $('form[name=actualizar] input[name=telefonoAlter]')[0].value;

            telefonoAlter = (telefonoAlter != "<?php echo $empresa['telefono_alternativo'] ?>" && telefonoAlter != '') ? telefonoAlter : "<?php echo $empresa['telefono_alternativo'] ?>";

            var contacto = $('form[name=actualizar] input[name=contacto]')[0].value;

            contacto = (contacto != "<?php echo $empresa['contacto'] ?>" && contacto != '') ? contacto : "<?php echo $empresa['contacto'] ?>";

            var direccionFiscal = $('form[name=actualizar] input[name=direccionFiscal]')[0].value;

            direccionFiscal = (direccionFiscal != "<?php echo $empresa['direccion_fiscal'] ?>" && direccionFiscal != '') ? direccionFiscal : "<?php echo $empresa['direccion_fiscal'] ?>";

            var cif = $('form[name=actualizar] input[name=cif]')[0].value;

            cif = (cif != "<?php echo $empresa['cif'] ?>" && cif != '') ? cif : "<?php echo $empresa['cif'] ?>";

            $.ajax({
                type: "POST",
                url: "<?php echo URL.$GLOBALS['language'];?>/Empresa/updateEmpresa",
                data: 
                {   
                    id: id,
                    nombreFiscal: nombreFiscal,  
                    email: email,
                    nombreComercial: nombreComercial,
                    telefono: telefono,
                    telefonoAlter: telefonoAlter,
                    contacto: contacto,
                    direccionFiscal: direccionFiscal,
                    cif: cif
                }
            })

                .done(function(){
                alert("¡Actualización efectuada con éxito!");
                document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/Perfil";
            })

                .fail(function(){
                alert("¡Error en la actualización!");
            })

        }

    });
</script>
<!-- Fin de la página visible para el rol 1 -->
<?php else: ?>
Atención, no tienes los privilegios necesarios para ver esta página.  
<a href="<?php echo URL ?>">Volver</a>
<?php endif;?>
</body>
</html>