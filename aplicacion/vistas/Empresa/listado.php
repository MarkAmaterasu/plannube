<?php
	include ('aplicacion/vistas/header.php');
?>
	<!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (sesion::getValue('ROL') == 1): ?>
    <?php
      // Recupera los trabajadores desde la base de datos.
      $trabajadores = $this->listarTrabajadores;
      // Crea una tabla con la información de los trabajadores anteriormente recuperado.
      echo <<< EOT
          <table>
            <tr> 
              <td></td>
              <td>NOMBRE</td>
            </tr>
EOT;

      foreach ($trabajadores as $trabajador) {
        echo <<< EOT

            <tr>
              <td></td>
              <td><a href="trabajador/$trabajador[idtrabajador]">$trabajador[nombre]</a></td>
            </tr>          

EOT;
    }
    echo "</table>";
    ?>
  </div>
<!-- Nav Derecha -->
   <?php
     // Menú lateral.
     include ('aplicacion/vistas/menuLateral.php');
   ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>  
        
<script>
  $(function(){
    $('#botonCerrarSesion').click(function(){
      document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
    });
  });
</script>
<?php else: ?>
  Atención, no tienes los privilegios necesarios para ver esta página.  
  <a href="<?php echo URL ?>">Volver</a> 
<?php endif ?>    
</body>
</html>