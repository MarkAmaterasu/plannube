<?php
	include ('aplicacion/vistas/header.php');
?>
  <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1): ?>
            <?php
            // Recupera los grupos desde la base de datos.
            $grupos = $this->listarGrupos;
            // Recupera los roles de la base de datos.
            $roles = $this->listarRoles;
            // Recupera los diferentes estados civiles de la base de datos.
            $estadosCiviles = $this->listarEstados;
            ?>
            <div class="row">
                <div class="col-lg-3 col-lg-offset-12 pull-right botonesEdit">
                    <input id="botonRegistro" name="botonRegistro" type="submit" value="Crear" class="btn btn-default"/>
                </div>
            </div>
          <!-- Formulario creación de trabajadores -->
          <form action="" method="POST" name="formRegistro" class="form-horizontal">
            <!-- Info personal requerido -->
            <div class="form-group">
                <label for="nombre"  class="col-lg-2 control-label">Nombre</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Nombre" name="nombre" id="nombre" class="form-control" required>  
                </div>
            </div>
            <div class="form-group">
                <label for="apellido1" class="col-lg-2 control-label">Primer Apellido</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Primer apellido" name="apellido1" id="apellido1" class="form-control" required> 
                </div>
            </div>
            <div class="form-group">
                <label for="apellido2" class="col-lg-2 control-label">Segundo Apellido</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Segundo apellido" name="apellido2" id="apellido2" class="form-control" required>       
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-lg-2 control-label">Email</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Correo electrónico" name="email" id="email" class="form-control" required>                
                </div>
            </div>
            <div class="form-group">
                <label for="dni" class="col-lg-2 control-label">NIF</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="NIF" name="dni" id="dni" class="form-control">        
                </div>
            </div>            
            <!-- Fin info personal requerida --> 
            <div class="form-group">
                <label for="pasaporte" class="col-lg-2 control-label">Pasaporte</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Pasaporte" name="pasaporte" id="pasaporte" class="form-control">       
                </div>
            </div>
            <div class="form-group">
                <label for="direccion" class="col-lg-2 control-label">Direccion</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Dirección" name="direccion" id="direccion" class="form-control">      
                </div>
            </div>
            <div class="form-group">
                <label for="cp" class="col-lg-2 control-label">Codigo Postal</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Código postal" name="cp" id="cp" class="form-control">      
                </div>
            </div>
            <div class="form-group">
                <label for="municipio" class="col-lg-2 control-label">Municipio</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Municipio" name="municipio" id="municipio" class="form-control">       
                </div>
            </div>
            <div class="form-group">
                <label for="provincia" class="col-lg-2 control-label">Provincia</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Provincia" name="provincia" id="provincia" class="form-control">     
                </div>
            </div>
            <div class="form-group">
                <label for="pais" class="col-lg-2 control-label">Pais</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Pais" name="pais" id="pais" class="form-control">       
                </div>
            </div>
            <div class="form-group">
                <label for="telFijo" class="col-lg-2 control-label">Teléfono fijo</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Teléfono fijo" name="telFijo" id="telFijo" class="form-control">       
                </div>
            </div>
            <div class="form-group">
                <label for="telMovil" class="col-lg-2 control-label">Teléfono movil</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Teléfono móvil" name="telMovil" id="telMovil" class="form-control">    
                </div>
            </div>
            <div class="form-group">
                <label for="telEmpresa" class="col-lg-2 control-label">Teléfono empresa</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Teléfono empresa" name="telEmpresa" id="telEmpresa" class="form-control">      
                </div>
            </div>
            <div class="form-group">
                <label for="banco" class="col-lg-2 control-label">Cuenta bancaria</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Cuenta bancaria" name="banco" id="banco" class="form-control">   
                </div>
            </div>
            <div class="form-group">
                <label for="hijos" class="col-lg-2 control-label">¿Hijos?</label>
                <div class="col-lg-10">
                    <select name="hijos" id="hijos" class="form-control">
                      <option value=""></option>
                      <option value="Si"> Sí </option>
                      <option value="No"> No </option>
                    </select>  
                </div>
            </div>
            <div class="form-group">
                <label for="cantHijos" class="col-lg-2 control-label">Nº Hijos</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Número de hijos" name="cantHijos" id="cantHijos" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="eCivil" class="col-lg-2 control-label">Estado Civil</label>
                <div class="col-lg-10">
                    <select name="eCivil" id="eCivil" class="form-control"> 
                      <option value=""></option> 
                    <?php
                    foreach ($estadosCiviles as $estado) {
                      echo <<< EOT
                      <option value="$estado[idestado_civil]">$estado[estado]</option>
EOT;
                    }
                    ?>              
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="sSocial" class="col-lg-2 control-label">Nº Seguridad Social</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Número seguridad social" name="sSocial" id="sSocial" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="hSemanal" class="col-lg-2 control-label">Horas semanales</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Horas semanales" name="hSemanal" id="hSemanal" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="dContrato" class="col-lg-2 control-label">Duración contrato</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Duración del contrato" name="dContrato" id="dContrato" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="tContrato" class="col-lg-2 control-label">Tipo de contrato</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Tipo de contrato" name="tContrato" id="tContrato" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="tPrueba" class="col-lg-2 control-label">Tiempo de prueba</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Tiempo de prueba" name="tPrueba" id="tPrueba" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="catSalarial" class="col-lg-2 control-label">Categoria Salarial</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Categoria salarial" name="catSalarial" id="catSalarial" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="finContrato" class="col-lg-2 control-label">Previsión fin de contrato</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Previsión fin de contrato" name="finContrato" id="finContrato" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="departamento" class="col-lg-2 control-label">Departamento</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Departamento" name="departamento" id="departamento" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="plus" class="col-lg-2 control-label">Cantidad Plus</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Cantidad plus" name="plus" id="plus" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="jefe" class="col-lg-2 control-label">Jefe</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="jefe" name="jefe" id="jefe" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="baja" class="col-lg-2 control-label">Baja</label>
                <div class="col-lg-10">
                    <select name="baja" id="baja" class="form-control">
                      <option value=""></option>
                      <option value="Si">Si</option>
                      <option value="No">No</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="tBaja" class="col-lg-2 control-label">Tipo  de baja</label>
                <div class="col-lg-10">
                    <select name="tBaja" id="tBaja" class="form-control"> 
                      <option value=""></option>
                      <option value="General">General</option>
                      <option value="Laboral">Laboral</option>
                      <option value="Maternidad">Maternidad / Paternidad</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="dSemanales" class="col-lg-2 control-label">Dias semanales</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="Dias semanales" name="dSemanales" id="dSemanales" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="rol" class="col-lg-2 control-label">Rol</label>
                <div class="col-lg-10">
                    <select name="rol" id="rol" class="form-control">
                      <option value=""></option>
                    <?php
                    foreach ($roles as $rol) {
                      echo <<< EOT
                      <option value="$rol[idrol]">$rol[nombreRol]</option>
EOT;
                     } 
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="grupo" class="col-lg-2 control-label">Grupo</label>
                <div class="col-lg-10">
                    <select name="grupo" id="grupo" class="form-control">
                    <option value=""></option> 
                    <?php
                    foreach ($grupos as $grupo) {
                      echo <<< EOT
                      <option value="$grupo[idgrupo]">$grupo[nombreGrupo]</option>
EOT;
                    }
                    ?>              
                    </select>
                </div>
            </div>
          </form>
        </div>
           <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>                 
        <script>

          $('#botonRegistro').click(function(e){      
              e.preventDefault();
              registroTrabajador();
          });

          // Función que se encarga, mediante ajax, del registro de los usuarios.
          function registroTrabajador(){
                        
              var nombre = $('form[name=formRegistro] input[name=nombre]')[0].value;
              var apellido1 = $('form[name=formRegistro] input[name=apellido1]')[0].value;
              var email = $('form[name=formRegistro] input[name=email]')[0].value;
              var apellido2 = $('form[name=formRegistro] input[name=apellido2]')[0].value;
              var rol = $('form[name=formRegistro] select[name=rol]')[0].value;
              var grupo = $('form[name=formRegistro] select[name=grupo]')[0].value;
              var dni = $('form[name=formRegistro] input[name=dni]')[0].value;
              var emailEmpresa = "<?php echo $empresa['email'] ?>";
              var empresa = "<?php echo $empresa['idempresa'] ?>";
                        
              $.ajax({
                  type: "POST",
                  url: "<?php echo URL.$GLOBALS['language']; ?>/Empresa/registroTrabajador",
                  data: 
                      {   
                        nombre: nombre, 
                        apellido1: apellido1, 
                        email: email, 
                        apellido2: apellido2,
                        rol: rol,
                        grupo: grupo,
                        dni: dni,
                        emailEmpresa: emailEmpresa,
                        empresa: empresa
                      }
              })

              .done(function(){
                  alert("¡Registro efectuado con éxito!"); 
                  $('form[name=formRegistro]')[0].reset(); 
              })

              .fail(function(){
                  alert("¡Error en el registro!");
              })
                        
          }

          // Función que se encarga de destruir la sesión.
          $(function(){
             $('#botonCerrarSesion').click(function(){
                document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
             });
          });
        </script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>