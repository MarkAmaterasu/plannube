<?php
	include ('aplicacion/vistas/header.php');
?>
  <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1): ?>
          <button id="crearGrupo">Crear grupo</button>
          <!-- Formulario creación de grupos -->
          <form action="" method="POST" id="formCrearGrupo" name="formCrearGrupo">
            <input type="text" placeholder="Nombre del grupo" id="nombreGrupo" name="nombreGrupo" required>
          </form>
        </div>
                   <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>          
        <script>

          $('#crearGrupo').click(function(e){      
              e.preventDefault();
              crearGrupo();
          });

          // Función que se encarga, mediante ajax, del registro de los usuarios.
          function crearGrupo(){
                        
              var nombre = $('form[name=formCrearGrupo] input[name=nombreGrupo]')[0].value;
              var empresa = "<?php echo $empresa['idempresa'] ?>";
                        
              $.ajax({
                  type: "POST",
                  url: "<?php echo URL.$GLOBALS['language']; ?>/Empresa/registroGrupo",
                  data: 
                      {   
                        nombre: nombre,
                        empresa: empresa
                      }
              })

              .done(function(){
                  alert("¡Grupo creado con éxito!");  
              })

              .fail(function(){
                  alert("¡Error en la creación del grupo!");
              })
                        
          }
            $(function(){
               $('#botonCerrarSesion').click(function(){
                  document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
               });
            });
        </script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>