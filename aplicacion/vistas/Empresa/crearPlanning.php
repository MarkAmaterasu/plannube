<?php
	include ('aplicacion/vistas/header.php');
?>
  <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1 || Sesion::getValue('ROL') == 2): ?>
      <button>Crear planning</button>
    </div>
           <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>         
        <script>
            $(function(){
               $('#botonCerrarSesion').click(function(){
                  document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
               });
            });
        </script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>