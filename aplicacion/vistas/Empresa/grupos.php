<?php
	include ('aplicacion/vistas/header.php');
?>
  <?php if (sesion::getValue('ROL') == 1): ?>
    <a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/crearGrupo">Crear grupo</a>
    <?php
      // Recupera los grupos desde la base de datos.
      $grupos = $this->listarGrupos;
      // Crea una tabla con la información de los grupos anteriormente recuperado.
      echo <<< EOT
          <table>
            <tr> 
              <td></td>
              <td>NOMBRE</td>
            </tr>
EOT;

      foreach ($grupos as $grupo) {
        echo <<< EOT

            <tr>
              <td></td>
              <td><a href="grupo/$grupo[idgrupo]">$grupo[nombreGrupo]</a></td>
            </tr>          

EOT;
    }
    echo "</table>";
    ?>
  </div>
           <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>      
        
<script>
  $(function(){
    $('#botonCerrarSesion').click(function(){
      document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
    });
  });
</script>
<?php else: ?>
  Atención, no tienes los privilegios necesarios para ver esta página.  
  <a href="<?php echo URL ?>">Volver</a> 
<?php endif ?>    
</body>
</html>