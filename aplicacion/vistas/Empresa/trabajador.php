<?php
	include ('aplicacion/vistas/header.php');
?>
    <!-- Comprobamos que solo accedan en la web los usuarios que tengan el Rol 1 -->
  <?php if (Sesion::getValue('ROL') == 1): ?>
    <button id="actualizar">Actualizar</button>
    <button id="atras">Atrás</button>
    <?php
      // Recupera los trabajadores desde la base de datos.
      $trabajador = $this->trabajador;
      // Crea una tabla con la información de los trabajadores anteriormente recuperado.
      echo <<< EOT
          <table>
            <tr> 
              <td></td>
              <td>NOMBRE</td>
              <td>USUARIO</td>
              <td>EMAIL</td>
            </tr>
EOT;


        echo <<< EOT

            <tr>
              <td></td>
              <td>$trabajador[nombre]</td>
              <td>$trabajador[nombreUsuario]</td>
              <td>$trabajador[email]</td>
            </tr>          

EOT;

    echo "</table>";
    ?>
  </div>
           <!-- Nav Derecha -->
               <?php
                  // Menú lateral.
                  include ('aplicacion/vistas/menuLateral.php');
                ?>
       </div>
    </div>
       <?php
            // Incluimos el footer a la vista.
            include("aplicacion/vistas/footer.php"); 
        ?>  
<script>
  $(function(){
    $('#botonCerrarSesion').click(function(){
      document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/destruirSesion/<?php echo $GLOBALS['language'] ?>";
    });

    $('#actualizar').click(function(){
      document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/actualizarTrabajador/<?php echo $trabajador['idtrabajador'] ?>";
    });

    $('#atras').click(function(){
      document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Empresa/listado";      
    });
    
  });
</script>
  <!-- Fin de la página visible para el rol 1 -->
  <?php else: ?>
      Atención, no tienes los privilegios necesarios para ver esta página.  
      <a href="<?php echo URL ?>">Volver</a>
  <?php endif;?>
</body>
</html>