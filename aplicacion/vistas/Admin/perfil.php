<?php
	include ('aplicacion/vistas/header.php');
?>
<body>
    Hola, esta és la página de perfil del administrador! <?php echo EJEMPLO ?><br>
    <!-- Menú superior -->
    <?php
    include ('aplicacion/vistas/menuSuperior.php');
    // Menú lateral.
    include ('aplicacion/vistas/menuLateral.php');
    ?>

    <div class="formWrapper">
            <?php echo Sesion::getValue('NOMBREUSUARIO'); ?>
    <?php
      // Recupera los trabajadores desde la base de datos.
      $trabajador = $this->trabajador;
      // Crea una tabla con la información de los trabajadores anteriormente recuperado.
      echo <<< EOT
          <table>
            <tr> 
              <td></td>
              <td>NOMBRE</td>
              <td>USUARIO</td>
              <td>EMAIL</td>
            </tr>
EOT;


        echo <<< EOT

            <tr>
              <td></td>
              <td>$trabajador[nombre]</td>
              <td>$trabajador[nombreUsuario]</td>
              <td>$trabajador[email]</td>
            </tr>          

EOT;

    echo "</table>";
    ?>
        </div>
        
        <script>
            $(function(){
               $('#botonCerrarSesion').click(function(){
                  document.location = "<?php echo (URL.$GLOBALS['language']); ?>/Trabajador/destruirSesion/<?php echo $GLOBALS['language'] ?>";
               });
            });
        </script>
</body>
</html>