<?php
	if (Sesion::existe()) {
		// Recupera los datos de la empresa desde la base de datos.
		$empresa = $this->datosEmpresa;
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Aplicación para definir plannings para los trabajadores.">
    <meta name="author" content="Marc Trallero García">
    <meta name="keywords" content="HTML5,CSS3,JQuery,Bootstrap">
    <title>Inicio</title>
    
    <!-- icono web-->
    <link rel="shortcut icon" href="<?php echo URL; ?>assets/img/paj.ico">

    <!-- Cargamos los estilos de Bootstrap -->
    <link rel="stylesheet" href="<?php echo URL; ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo URL; ?>assets/bootstrap/css/bootstrap-theme.min.css">
    
    <!-- Cargamos los estilos personalizados de Bootstrap -->
    <link rel="stylesheet" href="<?php echo URL; ?>assets/css/personalBootstrap.css">

    <!-- Cargamos los estilos personales --> 
    <link rel="stylesheet" href="<?php echo URL; ?>assets/css/estilos.css">

    <!-- Cargamos el Jquery y el JS de Bootstrap. -->
    <script src="<?php echo URL; ?>assets/js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo URL; ?>assets/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Cargamos el Jquery y el JS personal. -->
    <script src="<?php echo URL; ?>assets/js/mijs.js"></script> 
</head>
    <?php if (Sesion::existe()){?>
<body>
    <?php       
        // Menú superior.
        include ('aplicacion/vistas/menuSuperior.php'); 
    ?>
    <div class="container-fluid contenidoPrincipal">
       <div class="row">
          <!-- Contenido a la izquierda -->
           <div class="col-sm-5 col-sm-offset-1 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-2">
    <?php }?>