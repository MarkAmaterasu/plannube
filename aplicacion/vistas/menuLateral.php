<?php if (Sesion::getValue('ROL') == 1): ?>      
<div id="nav" class="col-xs-11 col-xs-offset-1 col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-3 col-lg-2 col-lg-offset-3 well navDerecha">
    <?php                 
        // Nombre del usuario conectado actualmente.
        echo HOLA . Sesion::getValue('NOMBREUSUARIO');
    ?>
    <!-- Menú lateral para el rol 1 (Empresa) -->
    <ul class="nav nav-pills nav-stacked ">
        <li role="presentation"><a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/insertarTrabajador"><?php echo AÑADIRTRABAJADORES ?></a></li>
        <li role="presentation"><a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/grupos"><?php echo GRUPOS ?></a></li>
        <li role="presentation"><a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/categorias"><?php echo CATEGORIAS ?></a></li>
        <li role="presentation"><a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/crearPlanning"><?php echo PLANOSDETRABAJO ?></a></li>
    </ul>	
</div>

<!-- Menú lateral para el rol 2 (Admin) y el rol 1 (Empresa) --> 
<?php elseif(Sesion::getValue('ROL') == 2 ): ?>
<a href="<?php echo URL.$GLOBALS['language']; ?>/Empresa/crearPlanning"><?php echo PLANOSDETRABAJO ?></a><br>

<!-- Menú lateral para el rol 3 (Trabajador) -->
<?php  else: ?>

<?php endif; ?>