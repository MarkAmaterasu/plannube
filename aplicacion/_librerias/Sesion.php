<?php

class Sesion {
	//Inicia de manera segura la sesión PHP.
	static function iniciarSesion() {
	    $session_name = 'sec_session_id';   // Configura un nombre de sesión personalizado.
	    $secure = SECURE;
	    // Esto detiene que JavaScript sea capaz de acceder a la identificación de la sesión.
	    $httponly = true;
	    // Obliga a las sesiones a solo utilizar cookies.
	    if (ini_set('session.use_only_cookies', 1) === FALSE) {
	        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
	        exit();
	    }
	    // Obtiene los params de los cookies actuales.
	    $cookieParams = session_get_cookie_params();
	    session_set_cookie_params($cookieParams["lifetime"],
	        $cookieParams["path"], 
	        $cookieParams["domain"], 
	        $secure,
	        $httponly);
	    // Configura el nombre de sesión al configurado arriba.
	    session_name($session_name);
	    session_start();            // Inicia la sesión PHP.
	    session_regenerate_id();    // Regenera la sesión, borra la previa. 
	}

	// Destruye la sesión PHP.
	static function destruirSesion(){
		// Desconfigura todos los valores de sesión.
		$_SESSION = array();
		 
		// Obtiene los parámetros de sesión.
		$params = session_get_cookie_params();
		 
		// Borra el cookie actual.
		setcookie(session_name(),
		        '',
		        time() - 42000, 
		        $params["path"], 
		        $params["domain"], 
		        $params["secure"], 
		        $params["httponly"]);
		 
		// Destruye sesión. 
		session_destroy();
		header('Location: ../ index.php');
	}

	// Inicializa un valor a una variable.
	static function setValue($variable, $valor){
		$_SESSION[$variable] = $valor;
	}

	// Coge el valor de uno de los indices de la array de sesión.
	static function getValue($variable){
		return $_SESSION[$variable];
	}

	// Borra el valor de la sesión específico sin destruirla.
	static function borrarValor(){
		// Si existe
		if (isset($SESSION[$variable])) {
			// Lo borramos
			unset($SESSION[$variable]);
		}
	}

	// Comprueba si existe la sesión.
	static function existe(){
		// Si el valor de la sesión es mayor que 0.
		if (sizeof($_SESSION) > 0) {
			return true;
		}else{
			return FALSE;
		}
	}
}
?>