<?php  
	class Controlador{

		function __construct(){
			Sesion::iniciarSesion();
			$this->vista = new Vista();
			$this->cargarModelo();
		}

		// Así cargamos el modelo de nuestra clase.
		function cargarModelo(){
			$modelo = get_class($this).'_modelo';
			$path = 'aplicacion/modelos/'.$modelo.'.php';
			if (file_exists($path)) {
				require $path;
				$this->modelo = new $modelo();
			}
		}
	}

?>