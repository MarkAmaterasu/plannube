<?php
	// Esta clase controlará la conexión a la base de datos y los trabajos sobre ella.
	class BaseDatos extends PDO{

		public function __construct($DB_HOST,$DB_USER,$DB_PASS,$DB_NAME,$DB_SYSTEM){

			parent::__construct($DB_SYSTEM.':host='.$DB_HOST.';dbname='.$DB_NAME,$DB_USER,$DB_PASS);

		}

		/**
		 * @param String $sql es la consulta.
		 * @param $array son los parámetros para hacer el bind.
		 * @param constant $fetchMode define el modo del fetch a utilizar de PDO.
		 * @return mixed.
		 */

		public function select($sql,$array = array(),$fetchMode = PDO::FETCH_ASSOC){

			$sentencia = $this->prepare($sql);

			foreach ($array as $key => $value) {
				// Vincula un valor al parámetro de sustitución con nombre o de signo de interrogación de la sentencia SQL que se utilizó para preparar la sentencia.
				$sentencia->bindValue("$key",$value);
			}

			$sentencia->execute();
			return $sentencia->fetchAll($fetchMode);
		}

		/**
		 * @param String $tabla La tabla donde insertar los datos.
		 * @param Array $datos Array de strings asociativos.
		 */		

		public function insert($tabla,$datos){
			// Ordena un array por clave, manteniendo la correlación entre la clave y los datos. 
			ksort($datos);
			// implode une elementos de un array en un string con glue (pegamento).
			// array_keys() devuelve las claves, numéricas y de tipo string, del array.
			// Recupera el nombre de los campos necesarios.
			$nombreCampos = implode(",", array_keys($datos));
			// Recupera el valor de los campos.
			$valorCampos = "'".implode("','", $datos)."'";

			$sentencia = $this->prepare("INSERT INTO $tabla ($nombreCampos) VALUES ($valorCampos)");

			return $sentencia->execute();			
			
		}

		/**
		 * @param String $tabla Nombre de la tabla.
		 * @param Array $datos Array de Strings asociativos.
		 * @param String $where Condición where de la consulta.
		 */

		public function update($tabla,$datos,$where){
			ksort($datos);
			$detalleCampo = NULL;
			foreach ($datos as $key => $value) {
				$detalleCampo .= "$key=:$key,";
			}
			// rtrim devuelve un string con los espacios en blanco retirados del final de str.
			$detalleCampo = rtrim($detalleCampo,',');
			$sentencia = $this->prepare("UPDATE $tabla SET $detalleCampo WHERE $where");

			foreach ($datos as $key => $value) {
				$sentencia->bindValue(":$key",$value);
			}

			return $sentencia->execute();
		}

		/**
		 * @param String $tabla Nombre de la tabla.
		 * @param String $where Condición where de la consulta.
		 * @param String $limite El Límite.
		 * @return mixed
		 */

		public function delete($tabla,$where,$limite = 1){
			return $this->exec("DELETE FROM $tabla WHERE $where LIMIT $limite");
		}
	}
?>