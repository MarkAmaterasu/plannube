<?php 
	/**
	 * @param String $controlador es el nombre del controlador a utilizar.
	 * @param String $vista es el nombre de la vista a cargar.
	 * Esta clase se encargará de renderizar, cargar la vista pasada.
	 */

	class Vista{
		function renderizar($controlador,$vista){
			$controlador = get_class($controlador);
			require VISTA.$controlador.'/'.$vista.'.php';
		}
	}

?>