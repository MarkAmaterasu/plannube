<?php  
	class Index extends Controlador{
		
		function __construct(){
			parent::__construct();
		}

		function index(){

			if (Sesion::existe()) {
				// Recupera del usuario que inicia la sesión su rol.
				$rol = Sesion::getValue('ROL');

				// Compara el rol y redirecciona al perfil adecuado.
				if ($rol == 1) {
					header("Location:".URL.$GLOBALS['language']."/Empresa/perfil");
				}elseif ($rol == 2) {
					header("Location:".URL.$GLOBALS['language']."/Admin/perfil");
				}elseif ($rol == 3) {
					header("Location:".URL.$GLOBALS['language']."/Trabajador/perfil");
				}else{
					$this->vista->renderizar($this,'index');
				}
			}else{
				$this->vista->renderizar($this,'index');
			}
		}

		function contacto(){
			$this->vista->renderizar($this,'contacto');
		}

		function envioContacto(){

			if (isset($_POST['nombre'])) {

				$nombre = $_POST['nombre'];
				$email = $_POST['email'];
				$mensaje = $_POST['mensaje'];
				$para = 'mark.amaterasu@gmail.com';
				$titulo = 'Contacto empresa interesada: Human Project';
				$header = 'From: ' . $email;
				$cabeceras = 'nombre:'.$nombre. "\r\n" .
						    'Email:'.$email. "\r\n" .
						    'Mensaje:'. $mensaje;

				if (isset($_POST['email']) && isset($_POST['nombre']) && isset($_POST['mensaje'])) {
					if(mail($para, $titulo, $cabeceras, $header)){
						echo "Envio correcto";
					}else{
						echo "Error en el envio";
					}
				}
			}
		}
	}
?>