<?php
	class Admin extends Controlador{

		function __construct(){
			parent::__construct();
		}

		// Método que creará la vista del perfil del trabajador.
		public function perfil(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->trabajador = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Crea la vista del perfil de la empresa.
				$this->vista->renderizar($this,'perfil');
			// Si no existe la sesión, redirigirá  la página principal.
			}else{
				header("Location:".URL);
			}
		}

		// Método que recogerá los datos de un formulario y actualizará los datos en la base de datos.
		public function actualizar(){
			if ($_POST["id"]) {
				
				$datos["nombre"] = $_POST["nombre"];
				$datos["nombreUsuario"] = $_POST["nombreUsuario"];
				$datos["email"] = $_POST["email"];
				$datos["contrasenya"] = $_POST["contrasenya"];

				return $this->modelo->actualizar($_POST["id"],$datos);

			}else{
				print("Error en la solicitud.");
			}
		}

		// Método que destruye la sesión.
		function destruirSesion($idioma){
			Sesion::destruirSesion();
			header('location:'.URL.$idioma."/Index/index");
		}
	}
?>