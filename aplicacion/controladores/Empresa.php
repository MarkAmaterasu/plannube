<?php 
	class Empresa extends Controlador{

		function __construct(){
			parent::__construct();
		}

		/////////////////////////////////////////////////////////////////////////////
		///////////////////////////	CREACIÓN VISTAS /////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////

		// Método que creará la vista del perfil de la empresa.
		public function perfil(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Crea la vista del perfil de la empresa.
				$this->vista->renderizar($this,'perfil');
			// Si no existe la sesión, redirigirá  la página principal.
			}else{
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página que listará a todos los trabajadores.
		public function listado(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores de los trabajadores.
				$this->vista->listarTrabajadores = $this->modelo->listarTrabajadores(Sesion::getValue("EMPRESA"));
				// Crea la vista.
				$this->vista->renderizar($this,'listado');	
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de un trabajador en particular.
		public function trabajador($id){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores del usuario en particular.
				$this->vista->trabajador = $this->modelo->trabajador($id)[0];
				// Crea la vista.
				$this->vista->renderizar($this,'trabajador');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de un trabajador en particular.
		public function insertarTrabajador(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores de los grupos.
				$this->vista->listarGrupos = $this->modelo->listarGrupos(Sesion::getValue("EMPRESA"));
				// Recoge los valores de los roles.
				$this->vista->listarRoles = $this->modelo->listarRoles();
				// Recoge los valores de los estados civiles.
				$this->vista->listarEstados = $this->modelo->listarEstados();
				// Crea la vista.
				$this->vista->renderizar($this,'insertarTrabajador');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de listado de grupos de trabajadores.
		public function grupos(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores de los grupos.
				$this->vista->listarGrupos = $this->modelo->listarGrupos(Sesion::getValue("EMPRESA"));
				// Crea la vista.
				$this->vista->renderizar($this,'grupos');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de listado de un grupo en particular de trabajadores.
		public function grupo($id){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores de los grupos.
				$this->vista->grupo = $this->modelo->grupo($id)[0];
				// Crea la vista.
				$this->vista->renderizar($this,'grupo');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de creación de grupos de trabajadores.
		public function crearGrupo(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Crea la vista.
				$this->vista->renderizar($this,'crearGrupo');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de actualización de los datos de un grupo.
		public function actualizarGrupo($id){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores del usuario en particular.
				$this->vista->grupo = $this->modelo->grupo($id)[0];
				// Crea la vista.
				$this->vista->renderizar($this,'actualizarGrupo');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de creación de grupos de trabajadores.
		public function crearPlanning(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Crea la vista.
				$this->vista->renderizar($this,'crearPlanning');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de actualización de los datos de un trabajador.
		public function actualizarTrabajador($id){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores del usuario en particular.
				$this->vista->trabajador = $this->modelo->trabajador($id)[0];
				// Recoge los valores de los grupos.
				$this->vista->listarGrupos = $this->modelo->listarGrupos(Sesion::getValue("EMPRESA"));
				// Recoge los valores de los roles.
				$this->vista->listarRoles = $this->modelo->listarRoles();
				// Recoge los valores de los estados civiles.
				$this->vista->listarEstados = $this->modelo->listarEstados();
				// Crea la vista.
				$this->vista->renderizar($this,'actualizarTrabajador');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de listado de categorias de trabajadores.
		public function categorias(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores de los grupos.
				$this->vista->listarCategorias = $this->modelo->listarCategorias();
				// Crea la vista.
				$this->vista->renderizar($this,'categorias');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de listado de una categoria en particular.
		public function categoria($id){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores de los grupos.
				$this->vista->categoria = $this->modelo->categoria($id)[0];
				// Crea la vista.
				$this->vista->renderizar($this,'categoria');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de actualización de los datos de un trabajador.
		public function crearCategoria(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Crea la vista.
				$this->vista->renderizar($this,'crearCategoria');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de actualización de los datos de una categoria.
		public function actualizarCategoria($id){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Recoge los valores del usuario en particular.
				$this->vista->categoria = $this->modelo->categoria($id)[0];
				// Crea la vista.
				$this->vista->renderizar($this,'actualizarCategoria');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		// Método que creará la vista de la página de actualización de los datos de la empresa.
		public function actualizarEmpresa(){
			// Comprueba que la sesión exista.
			if (Sesion::existe()) {
				// Recoge los valores del usuario conectado.
				$this->vista->datosUsuario = $this->modelo->trabajador(Sesion::getValue("ID"))[0];
				// Recoge los valores de la empresa del usuario conectado.
				$this->vista->datosEmpresa = $this->modelo->empresa(Sesion::getValue("EMPRESA"))[0];
				// Crea la vista.
				$this->vista->renderizar($this,'actualizarEmpresa');
			// Si no existe la sesión, redirigirá  la página principal.			
			}else{				
				header("Location:".URL);
			}
		}

		/////////////////////////////////////////////////////////////////////////////
		///////////////////////////	MÉTODOS /////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////

		// Método que recogerá los datos de un trabajador y actualizará los datos en la base de datos.
		public function updateCategoria(){
			if ($_POST["id"]) {
				
				$datos["nombre"] = $_POST["nombre"];

				return $this->modelo->actualizarCategoria($_POST["id"],$datos);

			}else{
				print("Error en la solicitud.");
			}
		}

		// Método que recogerá los datos de un trabajador y actualizará los datos en la base de datos.
		public function updateTrabajador(){
			if ($_POST["id"]) {
				
				$datos["nombre"] = $_POST["nombre"];
				$datos["email"] = $_POST["email"];

				return $this->modelo->actualizar($_POST["id"],$datos);

			}else{
				print("Error en la solicitud.");
			}
		}

		// Método que recogerá los datos de un grupo y actualizará los datos en la base de datos.
		public function updateGrupo(){
			if ($_POST["id"]) {
				
				$datos["nombreGrupo"] = $_POST["nombre"];

				return $this->modelo->actualizarGrupo($_POST["id"],$datos);

			}else{
				print("Error en la solicitud.");
			}
		}

		// Método que recogerá los datos de la empresa y actualizará los datos en la base de datos.
		public function updateEmpresa(){
			if ($_POST["id"]) {
				
				$datos["nombre_fiscal"] = $_POST["nombreFiscal"];
				$datos["nombre_comercial"] = $_POST["nombreComercial"];
				$datos["email"] = $_POST["email"];
				$datos["cif"] = $_POST["cif"];
				$datos["telefono"] = $_POST["telefono"];
				$datos["telefono_alternativo"] = $_POST["telefonoAlter"];
				$datos["contacto"] = $_POST["contacto"];
				$datos["direccion_fiscal"] = $_POST["direccionFiscal"];

				return $this->modelo->actualizarEmpresa($_POST["id"],$datos);

			}else{
				print("Error en la solicitud.");
			}
		}

		// Método que recogerá los datos del formulario de registro y los insertará en la base de datos.
		public function registro(){

			if (isset($_POST["nombre"]) && isset($_POST["nombreUsuario"]) && isset($_POST["email"]) && isset($_POST["contrasenya"])) {
				
				$datos["nombre"] = $_POST["nombre"];
				$datos["nombreUsuario"] = $_POST["nombreUsuario"];
				$datos["email"] = $_POST["email"];
				$datos["contrasenya"] = Cifrado::crear(ALGORITMO,$_POST["contrasenya"],LLAVE_CIFRADO) ;

				echo $this->modelo->registro($datos);
			}
		}

		// Método que recogerá los datos de la vista crearGrupo, y los insertará en la base de datos.
		public function registroGrupo(){
			if (isset($_POST["nombre"])) {
				// Guardamos los datos en un array asociativo.
				$datos["nombreGrupo"] = $_POST["nombre"];
				$datos["empresa"] = $_POST["empresa"];

				echo $this->modelo->registrarGrupo($datos);
			}
		}

		// Método que recogerá los datos de la vista insertarTrabajadores, los insertará en la BD y enviará un email al trabajador nuevo.				
		public function registroTrabajador(){

			if (isset($_POST["nombre"]) && isset($_POST["apellido1"]) && isset($_POST["email"])) {

				// Creamos un número de 4 dígitos aleatorios para la contraseña.
				$numeroAleatorio = rand(1000,9999);
				
				// Guardamos los datos recibidos en un array asociativo.
				$datos["nombre"] = $_POST["nombre"];
				$datos["apellido1"] = $_POST["apellido1"];
				$datos["apellido2"] = $_POST["apellido2"];
				$datos["email"] = $_POST["email"];
				$datos["dni"] = $_POST["dni"];
				$datos["empresa"] = $_POST["empresa"];

				// Si no se reciben datos sobre el rol del trabajador. Por defecto será 3 (trabajador).
				if ($_POST['rol'] == "") {
					$datos["rol"] = 3;
				}else{
					// En caso contrario, se cogerá lo que le pasen.
					$datos["rol"] = $_POST["rol"];
				}

				// Si no se reciben datos sobre el grupo donde esta un trabajador. Por defecto será 1 (Predeterminado).
				if ($POST['grupo'] == "") {
					$datos["grupo"] = 1;
				}else{
					// En caso contrario, se cogerá los datos pasados.
					$datos["grupo"] = $_POST["grupo"];
				}

				// Montamos el nombre de usuario.
				$nombreUsuario = $_POST["nombre"]{0}.$_POST["apellido1"];

				// Montamos la contraseña para el trabajador a registrar.
				$contrasenya = $_POST["nombre"]{0}.$_POST["apellido1"]{0}.$numeroAleatorio;
				$datos["contrasenya"] = Cifrado::crear(ALGORITMO,$contrasenya,LLAVE_CIFRADO);
				$datos["nombreUsuario"] = $nombreUsuario;
				
				// Se envia un correo electrónico al trabajador para que pueda acceder a la aplicación.
				$nombre = $_POST['nombre'];
				$apellido1 = $_POST['apellido1'];
				$email = $_POST['emailEmpresa']; // Email de la empresa.
				$mensaje = "Datos para acceder a la web: Nombre de usuario: ". $nombreUsuario . " Contraseña: " . $contrasenya . URL; // Mensaje de información.
				$para = $_POST['email']; // Email del trabajador.
				$titulo = 'Registro de la empresa.';
				$header = 'From: ' . $email;
				$cabeceras = 'Nombre:'.$nombre. "\r\n" .
						'Apellido:'.$apellido1. "\r\n" .
					    'Email:'.$email. "\r\n" .
					    'Mensaje:'. $mensaje;

				mail($para, $titulo, $cabeceras, $header);

				echo $this->modelo->registrarTrabajador($datos);
			}
		}

		// Método que recoge los datos del formulario de login, compara y crea la sesión.
		public function entrar(){

			if (isset($_POST["email"]) && isset($_POST["contrasenya"])) {
				
				$respuesta = $this->modelo->entrar(array(":email" => $_POST["email"]));
				$respuesta = $respuesta[0];
				if ($respuesta["contrasenya"] == Cifrado::crear(ALGORITMO,$_POST["contrasenya"],LLAVE_CIFRADO)) {
					$this->crearSesion($respuesta["nombreUsuario"],$respuesta["idtrabajador"],$respuesta["rol"],$respuesta["empresa"]);
					echo 1;
				}
			}
		}

		// Crea una sesión.
		function crearSesion($nombreUsuario,$id,$rol,$idempresa){
			Sesion::setValue('NOMBREUSUARIO',$nombreUsuario);
			Sesion::setValue('ID',$id);
			Sesion::setValue('ROL',$rol);
			Sesion::setValue('EMPRESA',$idempresa);
		}

		// Método que destruye la sesión.
		function destruirSesion($idioma){
			Sesion::destruirSesion();
			header('location:'.URL.$idioma."/Index/index");
		}
	}
?>