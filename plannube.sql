-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para humanproject
CREATE DATABASE IF NOT EXISTS `humanproject` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `humanproject`;


-- Volcando estructura para tabla humanproject.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='En esta tabla se guardarán las diferentes categorias.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.empresa
CREATE TABLE IF NOT EXISTS `empresa` (
  `idempresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_fiscal` varchar(200) NOT NULL COMMENT 'Nombre legal de la empresa.',
  `nombre_comercial` text COMMENT 'Nombre que puede usar la empresa de cara el público. Puede ser o no la misma que el nombre fiscal.',
  `cif` varchar(10) NOT NULL COMMENT 'Identificador de la empresa.',
  `email` text NOT NULL COMMENT 'Dirección de correo electrónico de la empresa.',
  `telefono` int(11) DEFAULT NULL COMMENT 'Telefono de contacto de la empresa.',
  `telefono_alternativo` text COMMENT 'Segundo numero de telefono posible de la empresa.',
  `contacto` text COMMENT 'Nombre de la persona a quien dirigirse para ponerse en contacto.',
  `direccion_fiscal` text COMMENT 'Donde se situa la empresa.',
  `nombre_autonomo` text NOT NULL COMMENT 'Nombre de la persona autónoma.',
  `apellido1` text NOT NULL COMMENT 'Primer apellido de la persona autónoma.',
  `apellido2` text NOT NULL COMMENT 'Segundo apellido de la persona autónoma.',
  `nif` varchar(15) NOT NULL COMMENT 'Número de identidad de la persona autónoma.',
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla guardará los datos de la empresa.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.estado_civil
CREATE TABLE IF NOT EXISTS `estado_civil` (
  `idestado_civil` int(11) NOT NULL AUTO_INCREMENT,
  `estado` text,
  PRIMARY KEY (`idestado_civil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla guardará los distintos estados civiles posibles.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.grupo
CREATE TABLE IF NOT EXISTS `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombreGrupo` text,
  `empresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`),
  KEY `FKempresa` (`empresa`),
  CONSTRAINT `FKempresa` FOREIGN KEY (`empresa`) REFERENCES `empresa` (`idempresa`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla guardará los datos de los distintos grupos.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.horario
CREATE TABLE IF NOT EXISTS `horario` (
  `idhorario` int(11) NOT NULL AUTO_INCREMENT,
  `idtrabajador` int(11) NOT NULL,
  `hora_inici` text,
  `hora_fi` text,
  `fecha` text,
  PRIMARY KEY (`idhorario`,`idtrabajador`),
  KEY `trabajadorFK` (`idtrabajador`),
  CONSTRAINT `trabajadorFK` FOREIGN KEY (`idtrabajador`) REFERENCES `trabajador` (`idtrabajador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla almacenará los datos de inicio-jornada // fi-jornada';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.rol
CREATE TABLE IF NOT EXISTS `rol` (
  `idrol` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador de rol.',
  `nombreRol` text COMMENT 'Nombre del rol.',
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla guardará los datos de los distintos roles posibles.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.tipo_baja
CREATE TABLE IF NOT EXISTS `tipo_baja` (
  `idtipo_baja` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text,
  PRIMARY KEY (`idtipo_baja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.tipo_contrato
CREATE TABLE IF NOT EXISTS `tipo_contrato` (
  `idtipo_contrato` int(11) NOT NULL,
  `codigo` int(11) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`idtipo_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla guardará los datos de los diferentes tipos de contratos.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla humanproject.trabajador
CREATE TABLE IF NOT EXISTS `trabajador` (
  `idtrabajador` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Numero identificador del trabajador.',
  `nombre` text NOT NULL COMMENT 'Nombre del trabajador.',
  `apellido1` text NOT NULL COMMENT 'Primer apellido del trabajador.',
  `apellido2` text COMMENT 'Segundo apellido del trabajador.',
  `nombreUsuario` text NOT NULL,
  `dni` text COMMENT 'DNI del trabajador.',
  `nie` text COMMENT 'NIE del trabajador en el caso de que sea extrangero.',
  `pasaporte` text COMMENT 'Pasaporte del trabajador.',
  `direccion` text COMMENT 'Dirección donde vive el trabajador.',
  `CP` int(11) DEFAULT NULL COMMENT 'Código postal donde vive.',
  `municipio` varchar(45) DEFAULT NULL,
  `provincia` text,
  `pais` text,
  `telefono_fijo` int(11) DEFAULT NULL,
  `telefono_movil` int(11) DEFAULT NULL,
  `telefono_empresa` int(11) DEFAULT NULL,
  `email` text,
  `cuenta_banco` text COMMENT 'Cuenta bancaria del empleado.',
  `hijos` varchar(5) DEFAULT NULL COMMENT 'Si tiene o no hijos.',
  `cant_hijos` int(11) DEFAULT NULL COMMENT 'Cantidad de hijos que tiene.',
  `estado_civil` int(11) DEFAULT NULL COMMENT 'El estado civil de la persona. Foreing key.',
  `seguridad_social` text COMMENT 'Número de la seguridad social.',
  `horas_semanales` int(11) DEFAULT NULL,
  `duracion_contrato` text,
  `codigo_contrato` int(11) DEFAULT NULL COMMENT 'Código del contrato. FK.',
  `tiempo_prueba` text,
  `categoria_salarial` int(11) DEFAULT NULL,
  `prevision_fin_contrato` text,
  `departamento` text,
  `plus` varchar(45) DEFAULT NULL,
  `jefe` int(11) DEFAULT NULL COMMENT 'Indica quien es su jefe. FK.',
  `baja` varchar(5) DEFAULT NULL COMMENT 'Está de baja o no.',
  `tipo_baja` int(11) DEFAULT NULL COMMENT 'Si está de baja, de que tipo. FK',
  `dias_semanales` int(11) DEFAULT NULL COMMENT 'Cantidad de dias a la semana que trabaja.',
  `rol` int(11) DEFAULT NULL COMMENT 'FK. El rol que tiene en la web.',
  `grupo` int(11) DEFAULT NULL,
  `empresa` int(11) DEFAULT NULL COMMENT 'Indica a la empresa a la cual pertenece.',
  `contrasenya` text,
  PRIMARY KEY (`idtrabajador`),
  KEY `FK_empresa` (`empresa`),
  KEY `FK_grupo` (`grupo`),
  KEY `FK_rol` (`rol`),
  KEY `FK_tipoBaja` (`tipo_baja`),
  KEY `TContrato` (`codigo_contrato`),
  KEY `FK_Estado_civil` (`estado_civil`),
  CONSTRAINT `FK_Estado_civil` FOREIGN KEY (`estado_civil`) REFERENCES `estado_civil` (`idestado_civil`),
  CONSTRAINT `FK_empresa` FOREIGN KEY (`empresa`) REFERENCES `empresa` (`idempresa`) ON UPDATE CASCADE,
  CONSTRAINT `FK_grupo` FOREIGN KEY (`grupo`) REFERENCES `grupo` (`idgrupo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_rol` FOREIGN KEY (`rol`) REFERENCES `rol` (`idrol`) ON UPDATE CASCADE,
  CONSTRAINT `FK_tipoBaja` FOREIGN KEY (`tipo_baja`) REFERENCES `tipo_baja` (`idtipo_baja`) ON UPDATE CASCADE,
  CONSTRAINT `TContrato` FOREIGN KEY (`codigo_contrato`) REFERENCES `tipo_contrato` (`idtipo_contrato`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabla guardará todos los datos referentes a los trabajadores.';

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
